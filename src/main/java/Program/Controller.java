package Program;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.sql.*;

public class Controller {
    private ObservableList<ObservableList> data;

    @FXML
    private Label lblStatus, lblgood, lblconfirm, lblconfirmed, lblWallet, lblMoney; //Inicijalizacija Labela

    @FXML
    private TextField txtUserName, txtPassword, txtField, txtMoney; //Inicijalizacija tekst fild

    @FXML
    private TableView tbv; //Inicijalizacija tabela

    //Otvara početni prozor za log in
    public void Login(ActionEvent event) throws Exception {
        if (txtUserName.getText().equals("user") && txtPassword.getText().equals("1234")) {
            Stage stage = new Stage(); //Pokretanje prozora
            FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("main.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 900, 600);
            stage.setScene(scene);
            stage.show();
        } else {
            lblStatus.setText("Incorrect username or password");
        }
    }

    public void OpenWallet(ActionEvent openwallet) throws Exception {
        Stage stage = new Stage(); //Pokretanje prozora
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("Wallet.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 300);
        stage.setScene(scene);
        stage.show();
    }

    public void WalletStatus(ActionEvent walletstatus) throws Exception {
        Connection connection = DriverManager.getConnection(DbConfig.url);
        Statement statement = connection.createStatement();
        String sqlwallet = "SELECT * FROM dbo.Wallet";
        ResultSet res = statement.executeQuery(sqlwallet);
        while (res.next()) {
            Integer wallet = res.getInt(1);
            String wallets = String.valueOf(wallet);
            lblWallet.setText("You currently have " + wallets + "$ in your wallet.");
        }
    }

    public void UpdateWallet(ActionEvent updatewallet) throws Exception {
        lblMoney.setText(null);
        String addAmount = txtMoney.getText();
        System.out.println(addAmount);
        if (addAmount.isEmpty())
            lblMoney.setText("Please enter an ammount first!");
        else {
            Connection connection = DriverManager.getConnection(DbConfig.url);
            Statement statement = connection.createStatement();
            String sqlupdate = "UPDATE dbo.Wallet SET Wallet_Status = Wallet_Status + " + addAmount;
            statement.execute(sqlupdate);
            lblMoney.setText("Monney succesfully added to your wallet!");
        }
    }

    //Otvara prozor sa Achievments
    public void Achievements(ActionEvent event2) throws Exception {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("achievements.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setScene(scene);
        stage.show();
    }

    //Otvara prozor sa Igrama
    public void Playgames(ActionEvent event3) throws Exception {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("playgames.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 800, 500);
        stage.setScene(scene);
        stage.show();
    }

    //Otvara prozor gdje se kupuju igre
    public void Buygames(ActionEvent event4) throws Exception {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("buygames.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 700, 450);
        stage.setScene(scene);
        stage.show();
    }

    //Ispisuje Achievments za odabranz igru, koje se upisuje u txt okvir
    public String sql;

    public void GetAch(ActionEvent event5) throws SQLException {
        if (txtField.getText().equals("CsGo")) {
            sql = "SELECT * FROM dbo.CsGo";
        } else if (txtField.getText().equals("Apex Legends")) {
            sql = "SELECT * FROM dbo.Apex";
        } else if (txtField.getText().equals("Call of Duty")) {
            sql = "SELECT * FROM dbo.CoD";
        } else if (txtField.getText().equals("Mario Kart")) {
            sql = "SELECT * FROM dbo.MarioK";
        } else if (txtField.getText().equals("FIFA 22")) {
            sql = "SELECT * FROM dbo.FIFA22";
        } else {
            lblgood.setText("Incorrect game name");
        }
        data = FXCollections.observableArrayList();
        Connection connection = DriverManager.getConnection(DbConfig.url);
        Statement statement = connection.createStatement();
        ResultSet res = statement.executeQuery(sql);

        for (int i = 0; i < res.getMetaData().getColumnCount(); i++) {
            final int j = i;
            TableColumn col = new TableColumn(res.getMetaData().getColumnName(i + 1));
            col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
                public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                    return new SimpleStringProperty(param.getValue().get(j).toString());
                }
            });
            tbv.getColumns().addAll(col);
            System.out.println("Column [" + i + "] ");
        }
        while (res.next()) {
            ObservableList<String> row = FXCollections.observableArrayList();
            for (int i = 1; i <= res.getMetaData().getColumnCount(); i++) {
                row.add(res.getString(i));
            }
            System.out.println("Row [1] added " + row);
            data.add(row);
        }
        tbv.setItems(data);
    }

    //Dodjeljuje string za određenu igru koju želimo kupiti
    public String addgame1, addgame2, addgame3, addgame4, addgame5;
    public String storestatus;

    public void Buy1(ActionEvent buy1) throws Exception {
        addgame1 = "INSERT INTO dbo.OwnedGames (Ime_Igre, Game_ID) \n" +
                "VALUES ('CsGo', 1);";
        storestatus = "CsGo";
        lblconfirm.setText("Are sure you want to buy CsGo?");
    }

    public void Buy2(ActionEvent buy2) throws Exception {
        addgame2 = "INSERT INTO dbo.OwnedGames (Ime_Igre, Game_ID) \n" +
                "VALUES ('Apex', 2);";
        storestatus = "Apex";
        lblconfirm.setText("Are sure you want to buy Apex?");
    }

    public void Buy3(ActionEvent buy3) throws Exception {
        addgame3 = "INSERT INTO dbo.OwnedGames (Ime_Igre, Game_ID) \n" +
                "VALUES ('Call of Duty', 3);";
        storestatus = "Call of Duty";
        lblconfirm.setText("Are sure you want to buy Call of Duty?");
    }

    public void Buy4(ActionEvent buy4) throws Exception {
        addgame4 = "INSERT INTO dbo.OwnedGames (Ime_Igre, Game_ID) \n" +
                "VALUES ('Mario Kart', 4);";
        storestatus = "Mario Kart";
        lblconfirm.setText("Are sure you want to buy Mario Kart?");
    }

    public void Buy5(ActionEvent buy5) throws Exception {
        addgame5 = "INSERT INTO dbo.OwnedGames (Ime_Igre, Game_ID) \n" +
                "VALUES ('FIFA 22', 5);";
        storestatus = "FIFA 22";
        lblconfirm.setText("Are sure you want to buy FIFA 22?");
    }

    //Kupnja igre
    public Integer gameID;
    public Integer c = 0, c2 = 0, c3 = 0, c4 = 0, c5 = 0;

    public void Confirm(ActionEvent event6) throws SQLException, IOException, Exception, SQLTimeoutException {
        lblconfirmed.setText(null);
        Connection connection = DriverManager.getConnection(DbConfig.url);
        Statement statement = connection.createStatement();
        String sql2 = "SELECT * FROM dbo.OwnedGames";
        String sql60 = "UPDATE dbo.Wallet SET Wallet_Status = Wallet_Status - 60";
        String sql40 = "UPDATE dbo.Wallet SET Wallet_Status = Wallet_Status - 40";
        String sql30 = "UPDATE dbo.Wallet SET Wallet_Status = Wallet_Status - 60";
        ResultSet res = statement.executeQuery(sql2);
        while (res.next()) {
            String brandName = res.getString("Ime_Igre");
            gameID = res.getInt(2);
            if (gameID == 1) {
                c++;
            }
            if (gameID == 2) {
                c2++;
            }
            if (gameID == 3) {
                c3++;
            }
            if (gameID == 4) {
                c4++;
            }
            if (gameID == 5) {
                c5++;
            }
        }
        if (c < 1 && storestatus == "CsGo") {
            statement.execute(sql30);
            statement.execute(addgame1);
            lblconfirmed.setText("The game is now in your library?");
        }
        if (c2 < 1 && storestatus == "Apex") {
            statement.execute(sql60);
            statement.execute(addgame2);
            lblconfirmed.setText("The game is now in your library?");
        }
        if (c3 < 1 && storestatus == "Call of Duty") {
            statement.execute(sql60);
            statement.execute(addgame3);
            lblconfirmed.setText("The game is now in your library?");
        }
        if (c4 < 1 && storestatus == "Mario Kart") {
            statement.execute(sql40);
            statement.execute(addgame4);
            lblconfirmed.setText("The game is now in your library?");
        }
        if (c5 < 1 && storestatus == "FIFA 22") {
            statement.execute(sql60);
            statement.execute(addgame5);
            lblconfirmed.setText("The game is now in your library?");
        }
    }

    //Pokretanje igre CsGo
    public void PlayCsGo(ActionEvent playCsGo) throws SQLException, IOException, Exception {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("CsGo.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 800, 500);
        stage.setScene(scene);
        Connection connection = DriverManager.getConnection(DbConfig.url);
        Statement statement = connection.createStatement();
        String sql2 = "SELECT * FROM dbo.OwnedGames";
        ResultSet res = statement.executeQuery(sql2);
        while (res.next()) {
            Integer game_ID = res.getInt(2);
            if (game_ID == 1) {
                stage.show();
            }
        }
    }

    //Pokretanje igre Apex Legends
    public void PlayApex(ActionEvent playApex) throws SQLException, IOException, Exception {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("Apex.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 800, 500);
        stage.setScene(scene);
        Connection connection = DriverManager.getConnection(DbConfig.url);
        Statement statement = connection.createStatement();
        String sql2 = "SELECT * FROM dbo.OwnedGames";
        ResultSet res = statement.executeQuery(sql2);
        while (res.next()) {
            Integer game_ID = res.getInt(2);
            if (game_ID == 2) {
                stage.show();
            }
        }
    }

    //Pokretanje igre Call of Duty
    public void PlayCoD(ActionEvent playCoD) throws SQLException, IOException, Exception {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("CoD.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 800, 500);
        stage.setScene(scene);
        Connection connection = DriverManager.getConnection(DbConfig.url);
        Statement statement = connection.createStatement();
        String sql2 = "SELECT * FROM dbo.OwnedGames";
        ResultSet res = statement.executeQuery(sql2);
        while (res.next()) {
            Integer game_ID = res.getInt(2);
            if (game_ID == 3) {
                stage.show();
            }
        }
    }

    //Pokretanje igre Mario Kart
    public void PlayMarioK(ActionEvent playMarioK) throws SQLException, IOException, Exception {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("MarioK.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 800, 500);
        stage.setScene(scene);
        Connection connection = DriverManager.getConnection(DbConfig.url);
        Statement statement = connection.createStatement();
        String sql2 = "SELECT * FROM dbo.OwnedGames";
        ResultSet res = statement.executeQuery(sql2);
        while (res.next()) {
            Integer game_ID = res.getInt(2);
            if (game_ID == 4) {
                stage.show();
            }
        }
    }

    //Pokretanje igre FIFA 22
    public void PlayFIFA22(ActionEvent playFIFA22) throws SQLException, IOException, Exception {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("FIFA22.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 800, 500);
        stage.setScene(scene);
        Connection connection = DriverManager.getConnection(DbConfig.url);
        Statement statement = connection.createStatement();
        String sql2 = "SELECT * FROM dbo.OwnedGames";
        ResultSet res = statement.executeQuery(sql2);
        while (res.next()) {
            Integer game_ID = res.getInt(2);
            if (game_ID == 5) {
                stage.show();
            }
        }
    }
}